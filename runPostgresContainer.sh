#!/bin/bash
containerName="pg_test_db"
compose=$(docker-compose up -d)

containerSearch=$(docker ps --filter name=$containerName --format "table {{.Names}}\t{{.Status}}")

checkRunning=$(echo $containerSearch | grep -c $containerName)

if [ $checkRunning -gt 0 ]; then
    echo ""
    echo "Waiting 2 seconds before checking containers are up, if you see 1 containers, all is working"
    sleep 2
    echo ""
    result=$(docker ps --filter name=$containerName)
    echo "$result"
    echo ""
    echo "  Connection URL: jdbc:postgresql://localhost:5432/pickerdb"
    echo "  Driver class name: org.postgresql.Driver"
    echo "	User: dbtest"
    echo "	Password: dbtest"
    echo ""
    echo "Connect through terminal"
    echo ""
    echo "  docker exec -it pg_test_db psql -U dbtest -W pickerdb"
    echo ""
    echo "PSQL commands"
    echo "   \q = quit "
    echo "   \dt = show tables "
    echo "   Basic select test = select * from countries;"
    echo ""
    echo "To remove the container and volumes run:"
    echo "   ./removeContainersAndVolumes.sh"
else
    echo "Containers not running. Check docker logs"
fi

