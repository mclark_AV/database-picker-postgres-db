# README #

### Simple postgres database for testing db picker ###



### How do I get set up? ###

* Run `runPostgresContainer.sh` to bring up the container

### How do I destroy? ###

* Run `removeContainersAndVolumes.sh` to remove the container and its data/volume
  
### Connect from Scriptrunner Resource  ###
  * Connection URL: `jdbc:postgresql://localhost:5432/pickerdb`
  * Driver class name: `org.postgresql.Driver`
  * User: `dbtest`
  * Password: `dbtest`
  
### Script field configuration options  ###

* Retrieval/validation SQL

```
select id, name, enabled from countries 
  where id = cast(? as numeric)
```

* Search SQL

```
select id, name, enabled from countries
  where name like ? || '%'
```
Configuration Script (optional)

`ConfigurationScriptForMultiDbPicker.groovy`  
  
###  Connect through terminal ###

  * `docker exec -it pg_test_db psql -U dbtest -W pickerdb`

### PSQL commands ###

   * `\q` = quit 
   * `\dt` = show tables 
   * Basic select test = `select * from countries;`


### SQL data ###

The data populated is defined in `init.sql`
