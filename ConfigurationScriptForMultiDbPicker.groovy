/**
 * This script will filter out the database entries that are set to disabled in the
 * enabled column
 */

import com.atlassian.jira.issue.Issue
import com.onresolve.scriptrunner.canned.jira.fields.editable.database.SqlWithParameters

getSearchSql = { String inputValue, Issue issue, originalValue ->
    def placeholders = getPlaceholderString(originalValue)
    new SqlWithParameters("select id, name, enabled from countries where name like ? || '%' and (enabled = true or cast(id as character) in ($placeholders)) order by name", [inputValue, *originalValue])
}

getValidationSql = { id, Issue issue, originalValue ->
    def placeholders = getPlaceholderString(originalValue)

    new SqlWithParameters("select id, name, enabled from countries where id = cast(? as numeric) and (enabled = true or ? in ($placeholders))", [id, id, *originalValue])
}

renderViewHtml = { displayValue, row ->
    displayValue + (row.enabled ? '' : ' (disabled)')
}