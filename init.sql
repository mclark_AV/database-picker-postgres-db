\c pickerdb;

CREATE TABLE IF NOT EXISTS countries (
  id SERIAL PRIMARY KEY,
  name text NOT NULl,
  enabled BOOLEAN NOT NULL
);

INSERT INTO countries(name, enabled) 
 VALUES
 ('France', 'Y'),
 ('Germany', 'Y'),
 ('Spain', 'N'),
 ('England', 'Y'),
 ('Canada', 'N');

